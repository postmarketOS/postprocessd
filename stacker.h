#ifndef POSTPROCESSD__STACKER_H
#define POSTPROCESSD__STACKER_H

#ifdef __cplusplus
extern "C" {
#endif

struct stacker {
        void *obj;
};
typedef struct stacker stacker_t;

stacker_t *
stacker_create(int verbose);

void
stacker_add_image(stacker_t *st, unsigned char *data, int width, int height);

char *
stacker_get_result(stacker_t *st);

char *
stacker_postprocess(stacker_t *st, unsigned char *data, int width, int height);

int
stacker_get_width(stacker_t *st);

int
stacker_get_height(stacker_t *st);

#ifdef __cplusplus
}
#endif

#endif //POSTPROCESSD__STACKER_H
